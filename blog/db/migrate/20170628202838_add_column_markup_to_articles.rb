class AddColumnMarkupToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :markup, :text
  end
end

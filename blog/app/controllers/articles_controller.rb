class ArticlesController < ApplicationController
	before_action :authenticate_user!, except: [:show, :index]

	before_action :authenticate_editor!, only: [:new, :create, :update]

	before_action :authenticate_admin!, only: [:destroy]

	#GET articles
	def index
		#Active record :)
		@articles = Article.paginate(page: params[:page], per_page:9).publicados.ultimos
	end

	#GET articles/:id
	def show
		@article = Article.find(params[:id])
		@comment = Comment.new
		@article.update_visit_counts
	end

	#GET articles/new
	def new
		#Hasta ahi no existe en BD
		@article= Article.new
	end

	#POST /articles
	def create
		#@article = Article.new(title: params[:article][:title],body: params[:article][:body])
		@article = current_user.articles.new(article_params)
		@article.categories = params[:categories]
		if @article.valid?
			@article.save
			redirect_to @article
		else
			#Le decimos que renderice la vista new.
			render :new
		end
	end

	#DELETE /article
	def destroy
		@article = Article.find(params[:id])
		@article.destroy #Elimina el objeto de la BD
		redirect_to articles_path
	end

	#GET /articles/:id/edit
	def edit
		@article = Article.find(params[:id])
	end

	#PUT /article/:id
	def update
		@article = Article.find(params[:id])
		if @article.update(article_params)
			redirect_to @article
		else
			render :edit
		end

	end

	#PUT /articles/:id/publish
	def publish
		@article = Article.find(params[:id])
		@article.publish!
		redirect_to @article
	end

	private 

	def validate_user
		redirect_to new_user_session_path, notice: 'Necesitas iniciar sesión'
	end

	def article_params
		params.require(:article).permit(:title, :body, :cover, :markup)
	end

end
module ApplicationHelper
	def article_cover url, options={}
		html_class = options[:class]
		html_style = " background:url(#{url});"\
					 " width:55%; height:200px;background-size:cover; margin: 0 auto;"
		html = "<header style='#{html_style}' class='#{html_class}'></header>"	
		
		html.html_safe		 		
	end
	
end

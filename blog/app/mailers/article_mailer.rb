class ArticleMailer < ActionMailer::Base
  def new_article(article)
  	@article = article

  	User.all.each do |user|
  		mail(to: user.email, subject: "Nuevo Articulo")
  	end
  end
end

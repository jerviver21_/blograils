json.extract! @comment, :id, :user_id, :article_id, :body
json.user do
	json.email @comment.user.email
end
